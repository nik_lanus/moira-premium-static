$(document).ready(function(){

    // Mobile navigation

    $('.js-toggle-nav').click(function(){
        $('body').toggleClass('nav-is-open');
    });



    // Search bar

    $('.js-search-input').keyup(function(){
        if ( $(this).val() ) {
            $(this).addClass('has-content');
            $('.js-search-submit').addClass('is-showing');
        } else {
            $(this).removeClass('has-content');
            $('.js-search-submit').removeClass('is-showing');
        }
    });



    // Home full page scrolling + slider

    $('.js-home-slider').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2500,
        dots: true,
        appendArrows: false,
        speed: 800,
        cssEase: 'cubic-bezier(0.770, 0.000, 0.175, 1.000)'
    });



    // Home full page scroll + footer and categories show/hide on scroll

    if ( $('body').hasClass('home') ) {
        $('.js-home').fullpage({
            sectionSelector: '.home-section',
            scrollBar: true,
            fitToSection: true,
            fitToSectionDelay: 400
        });

        $('.js-footer').addClass('footer--home');

        $('.js-last-section').waypoint(function(){
            $('.js-footer').toggleClass('is-showing');
        }, { offset: 100 });

        $('body').waypoint(function(){
            $('.js-categories').toggleClass('is-hidden');
        }, { offset: -5 });
    }



    // Lady load images

    $('.js-lazy').lazyload({
        effect: 'fadeIn',
        threshold: 2000
    });


    // Smooth scroll

    $('.js-smooth-scroll').smoothScroll();


    // Landing page

    $('.js-landing-slideshow').fullpage({
        sectionSelector: '.l-main',
        scrollBar: true,
        fitToSection: true,
        fitToSectionDelay: 400,
        menu: '.js-dots'
    });

    /*
    $('.js-home-composite').imagesLoaded( function() {
        setTimeout(function(){
            $('.js-home-composite span').velocity("transition.fadeIn", {
                duration: 2000,
                stagger: 300
            });
        }, 1000);
    });
    */

    $(".js-text-rotator").Morphist({
        speed: 2500,
        animateIn: 'fadeInDown',
        animateOut: 'fadeOutDown'
    });
});